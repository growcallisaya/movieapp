package com.grow.movieapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.grow.movieapp.adapters.MovieAdapter
import com.grow.movieapp.models.Movie

class MainActivity : AppCompatActivity(), MovieAdapter.OnItemClickListener, MainContract.View {

    private var mListOfMovies = ArrayList<Movie>()
    private lateinit var presenter : MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupToolbar()
        presenter = MainPresenter()
        presenter.attachView(this)
        presenter.displayMovies()
    }

    override fun loadProgressBar() {
        val mLoadingView = findViewById<LinearLayout>(R.id.loading_view)
        mLoadingView.visibility = View.VISIBLE
    }


    override fun onSuccessMovieResponse(movieList: ArrayList<Movie>?) {
        val mLoadingView = findViewById<LinearLayout>(R.id.loading_view)
        mLoadingView.visibility = View.GONE
        mListOfMovies = movieList!!
        displayList()
    }

    override fun onErrorMovieResponse() {
        val mLoadingView = findViewById<LinearLayout>(R.id.loading_view)
        val mErroMessageView = findViewById<LinearLayout>(R.id.error_message_view)
        mLoadingView.visibility = View.GONE
        mErroMessageView.visibility = View.VISIBLE
    }


    override fun displayList() {
        val rvMoviesList = findViewById<RecyclerView>(R.id.rv_movies_list)
        rvMoviesList.layoutManager =  LinearLayoutManager(this)
        rvMoviesList.adapter = MovieAdapter(mListOfMovies, this, this)
    }

    override fun onItemClick(movie: Movie) {
        startActivity(Intent(this, ChatActivity::class.java))
    }

    override fun setupToolbar() {
        val mToolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(mToolbar)
        supportActionBar.title = "Movies"
    }
}
