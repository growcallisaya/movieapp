package com.grow.movieapp

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import com.grow.movieapp.adapters.ChatAdapter
import com.grow.movieapp.models.Chat
import mehdi.sakout.fancybuttons.FancyButton
import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import androidx.appcompat.widget.Toolbar


class ChatActivity : AppCompatActivity(), ChatContract.View {

    private var mListOfChats = ArrayList<Chat>()
    private lateinit var mDatabase: DatabaseReference
    private lateinit var mChatAdaper: ChatAdapter
    private lateinit var recyclerChatList: RecyclerView
    private lateinit var presenter : ChatPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        mDatabase = FirebaseDatabase.getInstance().reference
        presenter = ChatPresenter()
        presenter.attachView(this)

        setupToolbar()
        validateEditMessage()
        submitMessageSetup()

    }

    private fun setupToolbar() {
        val mToolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(mToolbar)
        supportActionBar.setDisplayHomeAsUpEnabled(true)
        supportActionBar.setDisplayShowHomeEnabled(true)
        supportActionBar.title = "Inbox"
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onStart() {
        super.onStart()
        presenter.populateChat()
    }


    override fun setupChatView() {
        recyclerChatList = findViewById(R.id.rv_chats_list)
        recyclerChatList.layoutManager =  LinearLayoutManager(this)
        mChatAdaper = ChatAdapter(mListOfChats, this)
        recyclerChatList.adapter = mChatAdaper
    }


    override fun displayChats() {
        loadingProgressBar()
        presenter.displayChats()
    }

    override fun onCancelledResponse() {
        val mLoadingView = findViewById<LinearLayout>(R.id.loading_view)
        val mErroMessageView = findViewById<LinearLayout>(R.id.error_message_view)
        mLoadingView.visibility = View.GONE
        mErroMessageView.visibility = View.VISIBLE
    }

    override fun onChildAddedResponse(chat: Chat) {
        if (chat !== null) {
            mListOfChats.add(chat)
            mChatAdaper.notifyDataSetChanged()
        }
        val mLoadingView = findViewById<LinearLayout>(R.id.loading_view)
        mLoadingView.visibility = View.GONE

        showLastMessageOnList()
    }

    override fun resetScreen() {
        cleanInterface()
        hideKeyboard()
        showLastMessageOnList()
    }


    override fun loadingProgressBar() {
        val mLoadingView = findViewById<LinearLayout>(R.id.loading_view)
        mLoadingView.visibility = View.VISIBLE
    }

    override fun validateEditMessage() {
        val edtMessage = findViewById<EditText>(R.id.edt_message)
        edtMessage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                enableSubmitIfReady()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

    }

    override fun enableSubmitIfReady() {
        val btnSubmitMessage = findViewById<FancyButton>(R.id.btn_submit_message)
        val edtMessage = findViewById<EditText>(R.id.edt_message)
        val isReady = edtMessage.text.toString().length > 1
        btnSubmitMessage.isEnabled = isReady
    }

    override fun submitMessageSetup() {
        val btnSubmitMessage = findViewById<FancyButton>(R.id.btn_submit_message)
        val edtMessage = findViewById<EditText>(R.id.edt_message)
        btnSubmitMessage.setOnClickListener {
            val message = edtMessage.text.toString()
            presenter.submitMessage(message)
        }
    }

    override fun showLastMessageOnList() {
        val lastItemIndex = recyclerChatList.adapter.itemCount -1
        recyclerChatList.smoothScrollToPosition(lastItemIndex)

    }

    override fun cleanInterface() {
        val edtMessage = findViewById<EditText>(R.id.edt_message)
        edtMessage.setText("")
    }

    override fun hideKeyboard() {
        val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = this.currentFocus
        if (view == null) {
            view = View(this)
        }
        imm!!.hideSoftInputFromWindow(view!!.windowToken, 0)
    }


}
