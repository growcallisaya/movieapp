package com.grow.movieapp

import android.util.Log
import com.grow.movieapp.api.ApiManager
import com.grow.movieapp.api.ApiService
import com.grow.movieapp.models.Movie
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by growcallisaya on 1/7/19.
 */
class MainPresenter : MainContract.Presenter {

    var view: MainContract.View? = null
    private lateinit var mApiService : ApiService

    init {
        setupRetrofit()
    }

    override fun setupRetrofit() {
        val mApiManager = ApiManager.getInstance()
        mApiService = mApiManager.getApiServices()
    }

    override fun attachView(view: MainContract.View) {
        this.view = view
    }

    override fun displayMovies() {
        view!!.loadProgressBar()
        val request = mApiService.getMovies()
        request.enqueue(object : Callback<List<Movie>> {
            override fun onResponse(call: Call<List<Movie>>?, response: Response<List<Movie>>?) {
                Log.d("MovieResponse", response!!.body().toString())
                if(response!!.isSuccessful)
                    view!!.onSuccessMovieResponse(response.body() as ArrayList<Movie>)
            }
            override fun onFailure(call: Call<List<Movie>>?, t: Throwable?) {
                Log.d("MovieFailure", t!!.message)
                view!!.onErrorMovieResponse()
            }
        })
    }

}