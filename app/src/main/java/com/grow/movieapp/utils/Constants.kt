package com.grow.movieapp.utils

/**
 * Created by growcallisaya on 1/7/19.
 */
class Constants {

    companion object {
        val CHATS: String = "chats"
        val EMITTER: String = "You"
    }
}