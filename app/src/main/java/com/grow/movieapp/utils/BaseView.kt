package com.grow.movieapp.utils

import android.content.Context

/**
 * Created by growcallisaya on 1/7/19.
 */
interface BaseView {
    fun getContext(): Context
}