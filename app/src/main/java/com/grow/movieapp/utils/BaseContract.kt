package com.grow.movieapp.utils

/**
 * Created by growcallisaya on 1/7/19.
 */
class BaseContract {

    interface Presenter<T>{
        fun attachView(view:T)
    }

    interface View
}