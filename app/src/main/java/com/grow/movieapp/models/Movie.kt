package com.grow.movieapp.models

/**
 * Created by growcallisaya on 1/7/19.
 */
data class Movie(val title: String)