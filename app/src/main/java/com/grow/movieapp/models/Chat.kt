package com.grow.movieapp.models

/**
 * Created by growcallisaya on 1/7/19.
 */
data class Chat(val message: String? = "", val emitter: String? = "")