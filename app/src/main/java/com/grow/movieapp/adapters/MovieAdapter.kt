package com.grow.movieapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.grow.movieapp.R
import com.grow.movieapp.models.Movie


/**
 * Created by growcallisaya on 1/7/19.
 */
class MovieAdapter(val items : ArrayList<Movie>,
                   val context: Context,
                   val listener: OnItemClickListener) : RecyclerView.Adapter<MovieAdapter.MovieHolder>() {


    interface OnItemClickListener {
        fun onItemClick(movie: Movie)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_movie, parent, false)
        return MovieHolder(view)
    }

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
        val item = items[position]
        holder.bind(item, listener)
    }

    override fun getItemCount(): Int {
        return items.size
    }


    inner class MovieHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvMovieName = itemView.findViewById<TextView>(R.id.tv_movie_name)

        fun bind(movie: Movie, listener: OnItemClickListener) {
            tvMovieName.text = movie.title
            itemView.setOnClickListener { listener.onItemClick(movie) }
        }
    }
}