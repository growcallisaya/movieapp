package com.grow.movieapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.grow.movieapp.R
import com.grow.movieapp.models.Chat


/**
 * Created by growcallisaya on 1/7/19.
 */
class ChatAdapter(val items : ArrayList<Chat>,
                  val context: Context) : RecyclerView.Adapter<ChatAdapter.ChatHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_chats, parent, false)
        return ChatHolder(view)
    }

    override fun onBindViewHolder(holder: ChatHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return items.size
    }


    inner class ChatHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvChatMessage = itemView.findViewById<TextView>(R.id.tv_chat_message)
        val tvChatMessageEmitter = itemView.findViewById<TextView>(R.id.tv_chat_message_emitter)

        fun bind(chat: Chat) {
            tvChatMessage.text = chat.message
            tvChatMessageEmitter.text = chat.emitter
        }
    }
}