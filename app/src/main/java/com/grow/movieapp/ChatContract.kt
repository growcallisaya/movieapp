package com.grow.movieapp

import com.grow.movieapp.models.Chat
import com.grow.movieapp.utils.BaseContract

/**
 * Created by growcallisaya on 1/7/19.
 */
class ChatContract {

    interface Presenter : BaseContract.Presenter<View> {
        fun populateChat()
        fun displayChats()
        fun submitMessage(message: String)
    }

    interface View : BaseContract.View {
        fun setupChatView()
        fun displayChats()
        fun resetScreen()

        fun onChildAddedResponse(chat: Chat)
        fun onCancelledResponse()

        fun loadingProgressBar()
        fun validateEditMessage()
        fun enableSubmitIfReady()
        fun submitMessageSetup()
        fun showLastMessageOnList()
        fun cleanInterface()
        fun hideKeyboard()


    }
}