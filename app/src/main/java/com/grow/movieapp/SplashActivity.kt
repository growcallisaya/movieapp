package com.grow.movieapp

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by growcallisaya on 1/7/19.
 */
class SplashActivity: AppCompatActivity() {

    private val DURATION_SPLASH: Long = 2000
    private val DURATION_ANIMATION_ONE: Long = 8000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_splash)
        val imgLogo = findViewById<ImageView>(R.id.logo)
        val animationLogo = AnimationUtils.loadAnimation(this, R.anim.fadein)
        imgLogo.startAnimation(animationLogo)


        Handler().postDelayed({ imgLogo.clearAnimation() }, DURATION_ANIMATION_ONE)

        Handler().postDelayed({
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out)
            finish()
        }, DURATION_SPLASH)
    }
}