package com.grow.movieapp.api

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by growcallisaya on 1/7/19.
 */
class ApiManager {

    private var mService: ApiService? = null

    companion object {
        private var apiManager: ApiManager? = null

        fun getInstance(): ApiManager {
            if (apiManager == null) {
                apiManager = ApiManager()
                val retrofit = apiManager!!.initRetrofit()
                apiManager!!.initServices(retrofit)
                return apiManager!!
            }
            return apiManager!!
        }
    }




    fun initRetrofit(): Retrofit {

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.HEADERS
        interceptor.level = HttpLoggingInterceptor.Level.BODY


        val builder = OkHttpClient.Builder()
        builder.addInterceptor(interceptor)
        builder.addNetworkInterceptor(StethoInterceptor())

        val client = builder.build()


        val gson = GsonBuilder().setLenient().create()

        return Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
    }


    fun initServices(retrofit: Retrofit) {
        mService = retrofit.create(ApiService::class.java)
    }

    fun getApiServices(): ApiService {
        return mService!!
    }

}