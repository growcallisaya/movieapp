package com.grow.movieapp.api

import com.grow.movieapp.models.Movie
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by growcallisaya on 1/7/19.
 */

interface ApiService {
    @GET("https://tender-mclean-00a2bd.netlify.com/mobile/movies.json")
    fun getMovies(): Call<List<Movie>>

}
