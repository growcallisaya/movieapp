package com.grow.movieapp

import com.grow.movieapp.models.Movie
import com.grow.movieapp.utils.BaseContract

/**
 * Created by growcallisaya on 1/7/19.
 */
class MainContract {

    interface Presenter : BaseContract.Presenter<View> {
        fun setupRetrofit()
        fun displayMovies()
    }

    interface View : BaseContract.View {
        fun setupToolbar()
        fun loadProgressBar()
        fun onErrorMovieResponse()
        fun onSuccessMovieResponse(movieList: ArrayList<Movie>?)
        fun displayList()
    }
}