package com.grow.movieapp

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.grow.movieapp.models.Chat
import com.grow.movieapp.utils.Constants

/**
 * Created by growcallisaya on 1/7/19.
 */
class ChatPresenter: ChatContract.Presenter {

    var view: ChatContract.View? = null
    var mDatabase = FirebaseDatabase.getInstance().reference

    override fun attachView(view: ChatContract.View) {
        this.view = view
    }

    override fun populateChat() {
        view!!.setupChatView()
        view!!.displayChats()
    }

    override fun submitMessage(message: String) {
        val key = mDatabase.child(Constants.CHATS).push().key!!
        val newChat = Chat(message, Constants.EMITTER)
        mDatabase.child(Constants.CHATS).child(key).setValue(newChat)
        view!!.resetScreen()
    }

    override fun displayChats() {
        val query = mDatabase.child(Constants.CHATS)
        val eventListener = object: ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {
                val chat = dataSnapshot.getValue(Chat::class.java)
                view!!.onChildAddedResponse(chat!!)
            }
            override fun onCancelled(p0: DatabaseError) {
                view!!.onCancelledResponse()
            }
            override fun onChildMoved(p0: DataSnapshot, p1: String?) {}
            override fun onChildChanged(p0: DataSnapshot, p1: String?) {}
            override fun onChildRemoved(p0: DataSnapshot) {}
        }
        query.addChildEventListener(eventListener)
    }


}