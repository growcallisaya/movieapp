# Movie App for Turtle.Ai

My name is Grover, now I will explain you about the three following points:

- How to run the Code
- Explanation of architecture
- If you had more time, what would you like to improve?

### How to run the code

1. Get the file *movie_app_code.bundle*
2. Create a Folder *MovieApp*
3. Start Git:
```
git init
```
4. Run the following code:
```
git pull movie_app_code.bundle
```
5. Open the project with IDE Android Studio
6. Run the code in any device.


### Architecture

For this project we are using **MVP Architecture**. 
Basically is defined in three diffent parts:
- **Model** - Defines the data that will be shown.
- **Presenter** - Is the one who interact between the Model and the View. Get all the data and displays them to the View.
- **View** - Show all the data provided by the Presenter.


### If I had more time, what would you like to improve

1. **UX and UI** - As I'm a Graphic Designer the user experience is really important for me. So I could start improving the interface
and creating new ways to make it feels so unique. (Good experience, More conversions)

2. **Phone Resources** - using the tool "Android Profiler". It's really important use all the resources wisely. 
Of course, implementing Proguard to make the app smaller.

3. **Portability and Usability** - Testing in different kinds of devices (APIs), different kinds od brands.